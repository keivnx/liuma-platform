
/**
 * app测试定位方式
 **/
export const locateProps = {
    android: ["text","textContains","textMatches","textStartsWith","className","classNameMatches","description","descriptionContains","descriptionMatches","descriptionStartsWith","checkable","checked","clickable","longClickable","scrollable","enabled","focusable","focused","selected","packageName","packageNameMatches","resourceId","resourceIdMatches"],
    apple: ["id","className", "name", "value", "label"]
}  
